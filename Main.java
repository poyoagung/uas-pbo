import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
		System.out.println("Masukan Nomor Akun");
		Scanner scanner = new Scanner(System.in);
		String accNumber = scanner.nextLine();
		System.out.println("Masukan Nama");
		String accName = scanner.nextLine();
		BankAccount acc= new BankAccount(accNumber, accName);
		
		while(true){
			System.out.println("\n=============");
			System.out.println("Pilih Menu");
            System.out.println("1.Deposit");
            System.out.println("2.Withdraw");
            System.out.println("3.Checking Account");
            Scanner sc = new Scanner(System.in);
            int input = sc.nextInt();
            switch (input) {
            case 1: 
				System.out.println("Masukan jumlah deposit");
				int deposit= sc.nextInt();
				acc.deposit(deposit);
				System.out.println("Deposit sejumlah" +deposit+ " berhasil");
                break;
            case 2:  	
				System.out.println("Masukan jumlah withdraw");
				int withdraw= sc.nextInt();
				acc.withdraw(withdraw);
				System.out.println("withdraw sejumlah "+withdraw +" berhasil");
				break;
			case 3:  	
				System.out.println("Info Akun");
				System.out.println("Account Number : " + accNumber);
				System.out.println("Account Name : " + accName);
				
				SavingsAccount saving = new SavingsAccount(accNumber,accName, 10);
				
				double saldo = acc.getBalance();
				saving.deposit(saldo);
				System.out.println("Saldo sebelum kena bunga: " + saving.getBalance());
				saving.addInterest();
				System.out.println("Saldo setelah kena bunga: " + saving.getBalance())				
			break;
            default:
                System.out.println("Invalid input.");
            }}
	
	}
}